#!/usr/bin/php
<?php
$home = dirname(__FILE__);
if( !chdir( $home ) ){
    die("Cant chdir to " .$home."\n");
}
$db = new mysqli( MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_TABLE);
if ($db->connect_errno) {
    die('Could not connect: ' . $db->connect_error);
}

$cols['domain'] = 80;
$cols['created'] = 85;
$cols['error'] = 95;
$cols['notes'] = 84;
$cols['wp_user'] = 105;
$cols['wp_pass'] = 106;


$result = $db->query("
    SELECT DISTINCT
    d.item_id,
    c.meta_value,
    c.field_id,
    d.meta_value,
    wp_frm_fields.field_key,
    wp_frm_fields.`name`,
    wp_frm_fields.description
    FROM
    wp_frm_item_metas AS d
    LEFT JOIN wp_frm_item_metas AS c ON d.item_id = c.item_id
    INNER JOIN wp_frm_fields ON c.field_id = wp_frm_fields.id
    where d.field_id = 80
    ");


while($row= $result->fetch_row()){
    $ds[$row[0]][$row[2]] = $row[1];
    $ds[$row[0]]['item_id'] = $row[0];
    if(!isset($cols[$row[4]])){
        $cols[$row[4]] = $row[2];
    }
}
$i=0;
$newusers = [];
foreach($ds as $item){
    if(isset($item[$cols['created']])
        && ! preg_match('/Yes/i',$item[$cols['created']])){ continue;}

    if( preg_match('/^[A-Z0-9-.]+\.[A-Z0-9]{2,4}$/i',$item[$cols['domain']]) ){
        $error = array();
        foreach($cols as $k=>$v){
            if(!isset($item[$k]) && isset($item[$cols[$k]]) ){
                    $item[$k] = $item[$cols[$k]];
            }
        }
        $plugindir = '/var/www/vhosts/'.$item['domain'].'/httpdocs/wp-content/plugins';
        if(is_dir($plugindir)
           && !is_dir($plugindir.'/iwp-client')
           && file_exists('./iwp-client.zip')
        ){
                if(copy('./iwp-client.zip',
                    $plugindir.'/iwp-client.zip'
                )){
                    chdir($plugindir);
                    $zip = new ZipArchive;
                    $res = $zip->open('iwp-client.zip');
                    if ($res === TRUE) {
                        $zip->extractTo('.');
                        $zip->close();
                    }
                    unlink('iwp-client.zip');
                    shell_exec("chown $item[db_user].$item[db_user] iwp-client -R");
                }
        }elseif(is_dir($plugindir.'/iwp-client')){
            chdir($plugindir);
            shell_exec("chown $item[db_user].$item[db_user] iwp-client -R");
        }
        chdir($home);
        $newusers[$item['domain']] = $item;
    }
}
print_r($newusers);
exit;
?>
